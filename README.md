# Trivy CI

[Trivy](https://aquasecurity.github.io/trivy/v0.27.1/) を用いて、[Dockerfile](./docker/Dockerfile) をもとにビルドしたイメージの脆弱性を確認する CI

## CI stages

1. vuln-check
   - ビルドしたイメージの脆弱性を確認する
2. run-test
   - ビルドしたイメージの動作を確認する
3. image-push
   - ビルドしたイメージをコンテナレジストリにプッシュする

## Vulns example

脆弱性の出力例を以下に示す
```
2022-05-05T23:02:00.962Z	INFO	Detected OS: debian
2022-05-05T23:02:00.962Z	INFO	Detecting Debian vulnerabilities...
2022-05-05T23:02:00.978Z	INFO	Number of language-specific files: 1
2022-05-05T23:02:00.978Z	INFO	Detecting python-pkg vulnerabilities...
registry.gitlab.com/keitanoguchi/trivy-ci:ddcb207723714d8d77fc77256075d467231f8f1a (debian 10.12)
=================================================================================================
Total: 8 (CRITICAL: 8)
+----------+------------------+----------+-------------------+---------------+---------------------------------------+
| LIBRARY  | VULNERABILITY ID | SEVERITY | INSTALLED VERSION | FIXED VERSION |                 TITLE                 |
+----------+------------------+----------+-------------------+---------------+---------------------------------------+
| libc-bin | CVE-2021-33574   | CRITICAL | 2.28-10+deb10u1   |               | glibc: mq_notify does                 |
|          |                  |          |                   |               | not handle separately                 |
|          |                  |          |                   |               | allocated thread attributes           |
|          |                  |          |                   |               | -->avd.aquasec.com/nvd/cve-2021-33574 |
+          +------------------+          +                   +---------------+---------------------------------------+
|          | CVE-2021-35942   |          |                   |               | glibc: Arbitrary read in wordexp()    |
|          |                  |          |                   |               | -->avd.aquasec.com/nvd/cve-2021-35942 |
+          +------------------+          +                   +---------------+---------------------------------------+
|          | CVE-2022-23218   |          |                   |               | glibc: Stack-based buffer overflow    |
|          |                  |          |                   |               | in svcunix_create via long pathnames  |
|          |                  |          |                   |               | -->avd.aquasec.com/nvd/cve-2022-23218 |
+          +------------------+          +                   +---------------+---------------------------------------+
|          | CVE-2022-23219   |          |                   |               | glibc: Stack-based buffer             |
|          |                  |          |                   |               | overflow in sunrpc clnt_create        |
|          |                  |          |                   |               | via a long pathname                   |
|          |                  |          |                   |               | -->avd.aquasec.com/nvd/cve-2022-23219 |
+----------+------------------+          +                   +---------------+---------------------------------------+
| libc6    | CVE-2021-33574   |          |                   |               | glibc: mq_notify does                 |
|          |                  |          |                   |               | not handle separately                 |
|          |                  |          |                   |               | allocated thread attributes           |
|          |                  |          |                   |               | -->avd.aquasec.com/nvd/cve-2021-33574 |
+          +------------------+          +                   +---------------+---------------------------------------+
|          | CVE-2021-35942   |          |                   |               | glibc: Arbitrary read in wordexp()    |
|          |                  |          |                   |               | -->avd.aquasec.com/nvd/cve-2021-35942 |
+          +------------------+          +                   +---------------+---------------------------------------+
|          | CVE-2022-23218   |          |                   |               | glibc: Stack-based buffer overflow    |
|          |                  |          |                   |               | in svcunix_create via long pathnames  |
|          |                  |          |                   |               | -->avd.aquasec.com/nvd/cve-2022-23218 |
+          +------------------+          +                   +---------------+---------------------------------------+
|          | CVE-2022-23219   |          |                   |               | glibc: Stack-based buffer             |
|          |                  |          |                   |               | overflow in sunrpc clnt_create        |
|          |                  |          |                   |               | via a long pathname                   |
|          |                  |          |                   |               | -->avd.aquasec.com/nvd/cve-2022-23219 |
+----------+------------------+----------+-------------------+---------------+---------------------------------------+
Python (python-pkg)
===================
Total: 0 (CRITICAL: 0)
```
